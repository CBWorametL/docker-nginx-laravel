FROM nginx

ADD nginx.conf /etc/nginx/nginx.conf
ADD default.conf /etc/nginx/conf.d/default.conf
RUN usermod -u 1000 www-data
RUN usermod -G staff www-data
